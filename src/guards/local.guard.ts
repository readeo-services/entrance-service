import { CanActivate, ExecutionContext, Inject, Logger, OnModuleInit, Injectable } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { timeout } from 'rxjs/operators';
import { IAuthService, IAuthServiceTokenResponse } from '../msgInterfaces/auth.interface';

@Injectable()
export class LocalGuard implements CanActivate, OnModuleInit {
  private authService: IAuthService;

  onModuleInit(): void {
    this.authService = this.grpcClient.getService<IAuthService>('AuthService');
  }

  constructor(
    @Inject('AUTH_PACKAGE') private grpcClient: ClientGrpc,
  ) {}

  async canActivate(context: ExecutionContext): Promise<any> {
    const req = context.switchToHttp().getRequest();
    const email = req.body.email
    const password = req.body.password

    try {
      const res = await this.authService
        .authenticateUser({
          email,
          password
        })
        .pipe(timeout(5000))
        .toPromise<IAuthServiceTokenResponse>();

      req.accessToken = res.accessToken

      return res.accessToken
    } catch (err) {
      Logger.error(err);
      return false;
    }
  }
}
