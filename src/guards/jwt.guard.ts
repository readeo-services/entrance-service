import { CanActivate, ExecutionContext, Inject, Logger, OnModuleInit, Injectable } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { Metadata } from 'grpc'
import { timeout } from 'rxjs/operators';
import { IAuthService, IAuthServiceUserDataResponse } from '../msgInterfaces/auth.interface';

@Injectable()
export class JwtGuard implements CanActivate, OnModuleInit {
  private authService: IAuthService;

  onModuleInit(): void {
    this.authService = this.grpcClient.getService<IAuthService>('AuthService');
  }

  constructor(
    @Inject('AUTH_PACKAGE') private grpcClient: ClientGrpc,
  ) {}

  async canActivate(context: ExecutionContext): Promise<any> {
    const req = context.switchToHttp().getRequest();

    const metadata = new Metadata();
    metadata.set('apikey', 'test');
    metadata.set('apisecret', 'test');

    try {
      const res = await this.authService
        .validateToken({
          accessToken: req.headers['authorization']?.split(' ')[1]
        }, metadata)
        .pipe(timeout(5000))
        .toPromise<IAuthServiceUserDataResponse>();

      req.userId = res.userId

      return res.userId
    } catch (err) {
      Logger.error(err);
      return false;
    }
  }
}
