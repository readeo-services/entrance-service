import { Observable } from "rxjs";
import { Metadata } from 'grpc'

export interface IAuthService {
  validateToken(data: { accessToken: string }, metadata: Metadata): Observable<any>
  authenticateUser(data: { email: string, password: string }): Observable<any>
}

export interface IAuthServiceUserDataResponse {
  userId: string
}

export interface IAuthServiceTokenResponse {
  accessToken: string
}
