import { Observable } from "rxjs";

export interface IUserData {
  _id: string;
  email: string;
}

export interface IUserService {
  createUser(data: { email: string, password: string }): Observable<any>
}
