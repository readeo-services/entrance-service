import { ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

export const authMicroserviceOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    package: 'auth',
    url: 'readeo-service-auth:5000',
    protoPath: join(__dirname, '../../src/proto/auth.proto'),
  },
};

export const userMicroserviceOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    package: 'user',
    url: 'readeo-service-user:5000',
    protoPath: join(__dirname, '../../src/proto/user.proto'),
  },
};

export const tokenMicroserviceOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    package: 'token',
    url: 'readeo-service-token:5000',
    protoPath: join(__dirname, '../../src/proto/token.proto'),
  },
};


