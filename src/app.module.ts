import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientsModule } from '@nestjs/microservices';
import { authMicroserviceOptions } from './msgInterfaces/grpc.options';

@Module({
  imports: [ClientsModule.register([
    {
      name: 'AUTH_PACKAGE',
      ...authMicroserviceOptions,
    },
  ]),],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
