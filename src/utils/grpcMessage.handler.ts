import { RequestTimeoutException, Logger, ConflictException } from '@nestjs/common';
import { timeout, catchError } from 'rxjs/operators';
import { TimeoutError, throwError, Observable } from 'rxjs';

export class GRPCMessageHandler<T> {
  async request(action: Observable<any>): Promise<T> {
    try {
      return await action.pipe(
        timeout(5000),
        catchError(err => {
          if (err instanceof TimeoutError) {
            return throwError(new RequestTimeoutException());
          }
          // TODO: Implement all status codes
          // https://developers.google.com/maps-booking/reference/grpc-api/status_codes
          switch (err.code) {
            case 6:
              return throwError(new ConflictException(err.details))
            default:
              throwError(err)
          }
        }),
      ).toPromise<T>();
    } catch (e) {
      Logger.error(e)
      throw (e)
    }
  }
}
