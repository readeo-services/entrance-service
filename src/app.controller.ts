import { Controller, UseGuards, Request, Post, Body, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { LocalGuard } from './guards/local.guard';
import { IAuthServiceTokenResponse } from './msgInterfaces/auth.interface';
import { CreateUserDto } from './dto/createUser.dto';
import { IUserData } from './msgInterfaces/user.interface';
import { JwtGuard } from './guards/jwt.guard';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post("auth/login")
  @UseGuards(LocalGuard)
  async login(@Request() req: IAuthServiceTokenResponse): Promise<any> {
    return { accessToken: req.accessToken }
  }

  @Post("users")
  async createUser(@Body() params: CreateUserDto): Promise<IUserData> {
    return this.appService.createUser(params.email, params.password)
  }

  @Get("users")
  @UseGuards(JwtGuard)
  async getUsers(): Promise<IUserData[]> {
    // return this.appService.getUsers()
    console.log("ok:)")
    return null
  }
}
