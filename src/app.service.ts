import { Injectable, OnModuleInit } from '@nestjs/common';
import { IUserService, IUserData } from './msgInterfaces/user.interface';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { userMicroserviceOptions } from './msgInterfaces/grpc.options';
import { GRPCMessageHandler } from './utils/grpcMessage.handler';

@Injectable()
export class AppService implements OnModuleInit {
  private userService: IUserService;

  @Client(userMicroserviceOptions)
  private client: ClientGrpc

  onModuleInit(): void {
    this.userService = this.client.getService<IUserService>('UsersController');
  }

  async createUser(email: string, password: string): Promise<IUserData> {
    return await new GRPCMessageHandler<IUserData>()
          .request(this.userService.createUser({ email, password }))
  }
}
